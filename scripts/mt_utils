#!/bin/sh

############################################################################
# mt_utils
# --------
#
#   This file is part of MooseTeX.
#   Define some utility functions.
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

# does the same as "readlink -f" which is not supported on MacOS X Lion
readlinkrec()
{
    targetfile=$1
    curdir="`pwd`"
    case "`basename "$targetfile"`" in
	".." | "." | "/")
	    cd "$targetfile";
	    targetfile=""
	    ;;
	*)
	    cd "`dirname "$targetfile"`"
	    targetfile="`basename "$targetfile"`"
	    ;;
    esac
    while [ -L "$targetfile" ] ; do
	targetfile="`readlink "$targetfile"`"
	cd "`dirname "$targetfile"`"
	targetfile="`basename "$targetfile"`"
    done
    physdir="`pwd -P`"
    if [ "$physdir" = "/" ] ; then
	echo "/$targetfile"
    else
	echo "$physdir/$targetfile"
    fi
    cd "$curdir"
}

searchbin()
{
    binname=$1
    binpath="`which "$binname" 2>/dev/null`"
    if [ "$?" != "0" ] ; then
	return 1;
    fi
    echo "$binpath"
    return 0
}

searchlib()
{
    libname=$1
    for libpath in `echo $LD_LIBRARY_PATH | sed 's/:/ /g'` '/lib/' '/usr/lib/' '/usr/local/lib/'; do
	if ls "$libpath/$libname" > /dev/null 2> /dev/null; then
	    echo "$libpath"
	    return 0
	fi
    done
    return 1
}

is64bits()
{
    # not very elegant but uname -m does not seem to work for Mac Os X
    LD="`which ld`"
    LD="`readlinkrec $LD`"
    if file $LD | grep 64 >/dev/null 2>/dev/null ; then
	return 0
    else
	return 1
    fi
}

extract_version()
{
    FORMAT1_VERSION='^\([[:digit:]][[:digit:]]*\.[[:digit:]]*[[:digit:]]\)'
    FORMAT2_VERSION='[^[:digit:]\.]\([[:digit:]][[:digit:]]*\.[[:digit:]]*[[:digit:]]\)'
    FORMAT3_VERSION='^[^[:digit:]\.]\"\([[:digit:]][[:digit:]]*\.[[:digit:]]*[[:digit:]]\)\"'
    grep -e "$FORMAT1_VERSION" -e "$FORMAT2_VERSION" -e "$FORMAT3_VERSION" | \
	sed -e "s/.*$FORMAT1_VERSION.*/\1/" -e "s/.*$FORMAT2_VERSION.*/\1/" -e "s/.*$FORMAT3_VERSION.*/\1/" | \
	head -1 | sed 's \([^\.]*\.[^\.]*\).* \1 '
}
