#!/bin/bash

############################################################################
# mt_maketmpenv
# ------------
#
#   This file is part of MooseTeX.
#   Create a temporary file $tmpfile and directory $tmpdir that will be
#   erase after exit or interruption of a program that sources me.
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

if [ "$tmpdir" != "" ] || [ "$tmpfile" != "" ] ; then
    echo "Error: variable tmpdir or tmpfile already used ($(basename $0):$LINENO)" >&2
    exit 2
fi
source "$MT_SCRIPTS_PATH/mt_utils"

cleanup()
{
    if [ -f "$tmpdir/xvfb.lock" ] ; then
	kill $(head -1 $tmpdir/xvfb.lock) 2>/dev/null
    fi
    if [ "$tmpfile" != "" ] ; then
	rm -f "$tmpfile"*
    fi
    if [ "$tmpdir" != "" ] ; then
	rm -rf "$tmpdir"
    fi
    if [ "$tmpdir_lower" != "" ] ; then
	rm -rf "$tmpdir_lower"
    fi
}

bn="`basename $0`"
case "`uname`" in
    Darwin)
	declare -r tmpfile="`mktemp -t $bn`";
	declare -r tmpdir="`mktemp -d -t $bn`";
	;;
    *)
	declare -r tmpfile="`mktemp -t ${bn}_XXXXXX`";
	declare -r tmpdir="`mktemp -d -t ${bn}_XXXXXX`";
	;;
esac
tmpdir_lower="`echo "$tmpdir" | tr '[[:upper:]]' '[[:lower:]]'`"
mkdir -p "$tmpdir_lower"

trap "cleanup; exit;" EXIT INT TERM HUP CHLD SIGCHLD SIGINT SIGTERM SIGHUP
