############################################################################
# mt_makefile
# -----------
#
#   This file is part of MooseTeX. Define main dependencies and rules used
#   by MooseTeX to build LaTeX documents in PDF (and DVI and PS formats).
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

SHELL=/bin/bash

MAKEFILE_DEPS=$(CONF_PATH)/Makefile.deps

.SUFFIXES : .tex .pdf .ps .dvi
.PHONY : all nocolor target depend uselessimages vacuum clean distclean purge
.SILENT :

-include $(MAKEFILE_DEPS)
-include .moosetex/Makefile.deps

all: target

nocolor:
	unset TERM; make;

$(TARGETRULE):
	@# Four step compilation
	@# - Standard compilation
	@# - BBL file generation
	@# - Bibliography generation/Refs generation
	@# - Citations generation
	@if ! $(MT_SCRIPTS_PATH)/mt_latexcompile "$<" "$@" ; then						\
		printf "%-50.50s Compilation errors occurred\n" "$<" | $(COLORIZERED);				\
	   	rm -f "$@" "$(<:.tex=.aux)";									\
		exit 2;												\
	fi;													\
	if ! [ -f "$@" ] ; then											\
		printf "%-50.50s Compilation errors occurred\n" "$<" | $(COLORIZERED);				\
	   	rm -f "$@" "$(<:.tex=.aux)";									\
		exit 2;												\
	fi;

.ps.pdf:
	@printf "%-50.50s Conversion PS to PDF\n" "$@";								\
	$(PS2PDF) $(PS2PDF_SETTING) "$<" "$@";

.dvi.ps:
	@printf "%-50.50s Conversion DVI to PS\n" "$@";								\
	$(DVIPS) -Ppdf -D1200 -q -o "$@" "$<";									\
	$(MT_SCRIPTS_PATH)/mt_psquality "$@";

depend:
	@($(MT_SCRIPTS_PATH)/mt_texdeps . "$(IMAGES_SRC_PATH)" "$(TARGET_FORMAT)"				\
			 > "$(MAKEFILE_DEPS)") 2> $(CONF_PATH)/mt_deps.log;					\
	if [ -d "$(CONF_PATH)" ] && [ -d "`dirname "$(IMAGES_DIRECTIVES)"`" ] ; then				\
	   if ! [ -f "$(IMAGES_DIRECTIVES)" ] ; then touch "$(IMAGES_DIRECTIVES)"; fi;				\
	   touch "$(CONF_PATH)"/"`basename "$(IMAGES_DIRECTIVES)"`";						\
	   if [ -d "$(IMAGES_DST_PATH)" ] ; then								\
	     diff -d "$(CONF_PATH)"/"`basename "$(IMAGES_DIRECTIVES)"`" "$(IMAGES_DIRECTIVES)"			\
	     | grep '^[<>] ' | sed 's/[<>] \([^[:blank:]]*\).*/\1/' | grep -v '^[[:blank:]]*$$'			\
	     | while read -r file ; do										\
	    	find "$(IMAGES_DST_PATH)" -type f -name '*.'"$(TARGET_FORMAT)"					\
		| grep -E '.*/*'"$$file\.$(TARGET_FORMAT)"							\
		| while read -r pfile ; do									\
	            rm -f $$pfile;										\
	        done;												\
	     done;												\
	   fi;													\
	   cp -f "$(IMAGES_DIRECTIVES)" "$(CONF_PATH)"/"`basename "$(IMAGES_DIRECTIVES)"`";			\
	fi;

clean:
	@find . -name '*~' -type f -exec rm -f {} \;
	@find . -name '*.aux' -type f -exec rm -f {} \;
	@find . -name '*.toc' -type f -exec rm -f {} \;
	@find . -name '*.bbl' -type f -exec rm -f {} \;
	@find . -name '*.blg' -type f -exec rm -f {} \;
	@find . -name '*.bib.log' -type f -exec rm -f {} \;
	@find . -name '*.log' -type f -exec rm -f {} \;
	@find . -name '*.brf' -type f -exec rm -f {} \;
	@find . -name '*.ilg' -type f -exec rm -f {} \;
	@find . -name '*.ind' -type f -exec rm -f {} \;
	@find . -name '*.idx' -type f -exec rm -f {} \;
	@find . -name '*.backup' -type f -exec rm -f {} \;
	@find . -name '*.kilepr' -type f -exec rm -f {} \;
	@find . -name '*.out' -type f -exec rm -f {} \;
	@find . -name '*.nav' -type f -exec rm -f {} \;
	@find . -name '*.snm' -type f -exec rm -f {} \;
	@find . -name 'synctex.gz' -type f -exec rm -f {} \;
	@find . -name '*.vrb' -type f -exec rm -f {} \;
	@rm -f "$(MAKEFILE_DEPS)";
	@echo "Cleaning success" | $(COLORIZEGREEN);

distclean: clean
	@if [ -d "$(IMAGES_DST_PATH)" ] ; then									\
		cd "$(IMAGES_DST_PATH)" > /dev/null ;								\
		find "`pwd`" \( \( -type d -name ".*" -prune \)							\
			-o \( -type d -name "$(IMAGES_SRC_PATH)" -prune \) \)					\
			-o \( -type f \( -regex '.*\.eps' -or  -regex '.*\.pdf'	\)				\
			-exec rm -f {} \; \) ;									\
		cd - > /dev/null ;										\
	fi;													\
	rm -f *.dvi *.ps *.pdf;											\
	echo "Distcleaning success" | $(COLORIZEGREEN);

purge: distclean
	@rm -rf $(CONF_PATH);											\
	echo "Purge success" | $(COLORIZEGREEN);
