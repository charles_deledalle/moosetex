#!/bin/bash

############################################################################
# mt_embeddingfonts
# -----------------
#
#   This file is part of MooseTeX. Force fonts to be embedded in documents
#   with PDF or EPS formats.
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

if [ $# -lt 1 ] ; then
    echo "Usage: `basename $0` filein.(eps|pdf) [fileout.(eps|pdf)]"
    exit 1
fi
source "$MT_SCRIPTS_PATH/mt_utils"
source "$MT_SCRIPTS_PATH/mt_maketmpenv"

filein=$1;
if [ $# -lt 2 ] ; then
    fileout=$filein;
else
    fileout=$2;
fi
iext="`$MT_SCRIPTS_PATH/mt_getextension "$filein"`";
oext="`$MT_SCRIPTS_PATH/mt_getextension "$fileout"`";

# Retrieve margins (in bp)
left="`$MT_SCRIPTS_PATH/mt_unitconversion   "$margin_left"   "$margin_unit" "bp"`";
top="`$MT_SCRIPTS_PATH/mt_unitconversion    "$margin_top"    "$margin_unit" "bp"`";
right="`$MT_SCRIPTS_PATH/mt_unitconversion  "$margin_right"  "$margin_unit" "bp"`";
bottom="`$MT_SCRIPTS_PATH/mt_unitconversion "$margin_bottom" "$margin_unit" "bp"`";

# Retrieve size (in px)
width="`$MT_SCRIPTS_PATH/mt_unitconversion   "$size_width"   "$size_unit" "cm"`";
height="`$MT_SCRIPTS_PATH/mt_unitconversion  "$size_height"  "$size_unit" "cm"`";

# Reduce size in case it doesn't fit the page
if [ "`echo "$width > 100" | bc`" == "1" ] ; then
    height="`echo "(100 * (100 * $height) / $width) / 100" | bc`"
    width=100
    echo "Too large: automatic rescaling to 100cm width"
fi
if [ "`echo "$height > 100" | bc`" == "1" ] ; then
    width="`echo "(100 * (100 * $width) / $height) / 100" | bc`"
    height=100
    echo "Too large: automatic rescaling to 100cm height"
fi

# Embed fonts
case "$iext" in
    pdf)
	if pdffonts "$filein" | tail -n +3 | cut -b 56-58 | grep -q -v "yes" ; then
	    if ! $PDFTOPS -level3 -eps "$filein" "$tmpfile.eps"; then
		echo "Pdf2ps encountered an error ($(basename $0):$LINENO)" 1>&2
		exit 2;
	    fi
	    if ! $MT_SCRIPTS_PATH/mt_psquality "$tmpfile.eps" ; then
		exit 2;
	    fi
	    if ! $PS2PDF14 $PS2PDF_SETTING $PS2PDF_OPTIONS "$tmpfile.eps" "$tmpfile.pdf" >&2 >/dev/null ; then
		echo "Ps2pdf14 encountered an error ($(basename $0):$LINENO)" 1>&2
		exit 2;
	    fi
	else
	    if [ "$filein" == "$fileout" ] ; then
		cp -f "$filein" "$tmpfile.pdf"
	    else
		ln -s "`readlinkrec $filein`" "$tmpfile.pdf"
	    fi
	fi
	;;
    eps)
	if ! $MT_SCRIPTS_PATH/mt_psquality "$filein" ; then
	    exit 2;
	fi
    	if ! $PS2PDF14 $PS2PDF_SETTING $PS2PDF_OPTIONS "$filein" "$tmpfile.pdf" 2> /dev/null; then
	    echo "Ps2pdf14 encountered an error ($(basename $0):$LINENO)" 1>&2
	    exit 2;
	fi
	;;
esac

# Find smallest bounding box
if ! $PDFCROP --clip "$tmpfile.pdf" "$tmpfile.crop.pdf" > /dev/null ; then
    echo "Pdfcrop encountered an error ($(basename $0):$LINENO)" 1>&2
    exit 2;
fi
if [ "$MOOSETEX_DEBUG_DISPLAY" == "1" ] ; then
    display "$tmpfile.crop.pdf"
fi

# Resize to targeted size
rm -f "$tmpfile.pdf"
cat > "$tmpfile.tex" <<%
\documentclass[10pt,a0paper]{memoir}
\setstocksize{110cm}{110cm}
\settrimmedsize{109cm}{109cm}{*}
\settrims{0.5cm}{0.5cm}
\setlrmarginsandblock{2cm}{2cm}{*}%%%%
\setulmarginsandblock{2cm}{2cm}{*}
\checkandfixthelayout
\fixpdflayout
\usepackage{graphicx}
\usepackage{grffile}
\pagestyle{empty}
\begin{document}
\centering
\includegraphics[width=${width}cm,keepaspectratio,clip]{$tmpfile.crop}
\end{document}
%

LATEXFLAGS="-interaction=batchmode -shell-escape"
if ! $PDFLATEX $LATEXFLAGS -output-dir="`dirname "$tmpfile.tex"`" "$tmpfile.tex" >&2 >/dev/null ; then
    echo "Pdflatex encountered an error ($(basename $0):$LINENO)" 1>&2
    exit 2;
fi

# Crop again and add margins
if ! $PDFCROP --clip --margins "$left $top $right $bottom" \
    "$tmpfile.pdf" "$tmpfile.crop.pdf" > /dev/null ; then
    echo "Pdfcrop encountered an error ($(basename $0):$LINENO)" 1>&2
    exit 2;
fi
if [ "$MOOSETEX_DEBUG_DISPLAY" == "1" ] ; then
    display "$tmpfile.crop.pdf"
fi

# Convert to targeted format
case "$oext" in
    pdf)
	if ! mv -f "$tmpfile.crop.pdf" "$fileout" ; then
	    echo "Mv encountered an error ($(basename $0):$LINENO)" 1>&2
	    exit 2;
	fi

	;;
    eps)
	if ! $PDFTOPS -level3 -eps "$tmpfile.crop.pdf" "$fileout" ; then
	    echo "Pdftops encountered an error ($(basename $0):$LINENO)" 1>&2
	    exit 2;
	fi
	$MT_SCRIPTS_PATH/mt_psquality "$fileout";
esac
if [ "$MOOSETEX_DEBUG_DISPLAY" == "1" ] ; then
    display "$fileout"
fi
