/*
** list_find_item_eq.c for list in /u/a1/deleda_c/rendu/piscine/j6/list
**
** Made by charles-alban deledalle
** Login   <deleda_c@epita.fr>
**
** Started on  Fri Sep 16 22:46:55 2005 charles-alban deledalle
** Last update Sat Sep 17 03:06:03 2005 charles-alban deledalle
*/

#include <stdlib.h>
#include "s_list.h"

void	*list_find_item_eq(const struct s_list	*list,
			   const void		*data,
			   t_cmp_func		cmp_func)
{
  while (list && cmp_func(data, list->data))
    list = list->next;
  if (list)
    return list->data;
  else
    return NULL;
}
