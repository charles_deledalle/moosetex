/*******************************************************************************
 * s_list.h
 * --------
 *
 *   This file is part of MooseTeX. Provide definitions for list structures and
 *   manipulation functions.
 *
 *   Copyright (c) 2013-2015 Charles Deledalle
 *
 * License
 *
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use,
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info".
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability.
 *
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or
 *   data to be ensured and, more generally, to use and operate it in the
 *   same conditions as regards security.
 *
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *
 * Contact
 *
 *   contributor(s): Charles Deledalle
 *
 *   charles (dot) deledalle (at) gmail (dot) com
 *
 ******************************************************************************/

#ifndef S_LIST_H_
# define S_LIST_H_

struct             s_list
{
  struct s_list   *next;
  void            *data;
};

typedef int	(*t_cmp_func)(const void *, const void*);
typedef void	(*t_free_func)(void *);

void		list_add_item_end(struct s_list **list, void *data);
void		list_add_item_head(struct s_list **list, void *data);
void		list_add_item_sorted(struct s_list **list,
				     void		*data,
				     t_cmp_func		cmp_func,
				     t_free_func free_func);


void		*list_find_item_eq(const struct s_list	*list,
				   const void		*data,
				   t_cmp_func		cmp_func);

void		list_free(struct s_list **list, t_free_func free_func);

#endif /* ! S_LIST_H_ */
