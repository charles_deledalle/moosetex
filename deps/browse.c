/***************************************************************************
 * browse.c
 * --------
 *
 *   This file is part of MooseTeX. Browse a directory and store all file
 *   informations in a list.
 *
 *   Copyright (c) 2013-2015 Charles Deledalle
 *
 * License
 *
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use,
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info".
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability.
 *
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or
 *   data to be ensured and, more generally, to use and operate it in the
 *   same conditions as regards security.
 *
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *
 * Contact
 *
 *   contributor(s): Charles Deledalle
 *
 *   charles (dot) deledalle (at) gmail (dot) com
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#ifdef ARCHI_NetBSD
# include <sys/dirent.h>
#else /* !ARCHI_NetBSD */
# include <dirent.h>
#endif /* !ARCHI_NetBSD */
#include "browse.h"
#include "list/s_list.h"
#include "fileinfo.h"

struct			s_spth
{
  const char		*sbrowse;
  const char		*path;
};

int			perror_fail(const char		*sbrowse,
				    const char		*path)
{
  write(STDERR_FILENO, sbrowse, strlen(sbrowse));
  write(STDERR_FILENO, ": ", 2);
  perror(path);
  return 1;
}

char*			mt_strndup(const char *s, size_t n)
{
  char* res;
  res = strdup(s);
  res[n] = '\0';
  return res;
}

static int		check_match(const char		*fn,
				    const char		*absfn)
{
  absfn = absfn;
  if (!strncmp(fn, "Makefile", 8))
    return 0;
  if (!strcmp(fn, "directives.conf"))
    return 0;
  if (fn[0] == '.')
    return 0;
  if (fn[strlen(fn)-1] == '~')
    return 0;
  return 1;
}

static struct s_list*	browse_rec(struct s_list *res,
				   const char	*sbrowse,
				   int		len_opath,
				   const char	*path);

static struct s_list*	browse_file(struct s_list*res,
				    struct s_spth	*spth,
				    int		len_opath,
				    DIR		*dir)
{
  struct dirent		*dp;
  struct stat		sb;
  struct fileinfo	*fi;
  char			*ext;

  while ((dp = readdir(dir)))
    {
      fi = malloc(sizeof(struct fileinfo));
      fi->dn = strdup(spth->path);
      fi->bn = strdup(dp->d_name);
      fi->absfn = strdup(fi->dn);
      fi->absfn =
	realloc(fi->absfn,
		(strlen(fi->absfn) + 1 + strlen(fi->bn) + 1) *
		sizeof(char));
      fi->absfn = strcat(fi->absfn, "/");
      fi->absfn = strcat(fi->absfn, fi->bn);
      for (ext = fi->bn + strlen(fi->bn);
	   ext != fi->bn && *ext != '.' && *ext != '/';
	   ext--)
	;
      if (*ext == '/')
	fi->ext = strdup("");
      else
	if (*ext == '.')
	  fi->ext = strdup(ext);
	else
	  fi->ext = strdup("");
      if ((int) strlen(fi->dn) < len_opath + 1)
	fi->reldn = strdup("");
      else
	fi->reldn = strdup(fi->dn + len_opath + 1);
      fi->relfn = mt_strndup(fi->absfn + len_opath + 1, strlen(fi->absfn) - len_opath - 1);
      fi->relfn_noext = mt_strndup(fi->absfn + len_opath + 1, strlen(fi->absfn) - len_opath - strlen(fi->ext) - 1);
      fi->bn_noext = mt_strndup(fi->bn, strlen(fi->bn) - strlen(fi->ext));
      if (lstat(fi->absfn, &sb) == -1)
	{
	  free_fileinfo(fi);
	  return NULL;
	}
      fi->used_files = NULL;
      if (!(strcmp(fi->bn, ".") && strcmp(fi->bn, "..")))
	free_fileinfo(fi);
      else
	if (!check_match(fi->bn, fi->absfn))
	  free_fileinfo(fi);
	else
	  if (S_ISDIR(sb.st_mode) && !S_ISBLK(sb.st_mode) &&
	      !S_ISLNK(sb.st_mode) && !S_ISSOCK(sb.st_mode))
	    {
	      res = browse_rec(res, spth->sbrowse, len_opath, fi->absfn);
	      free_fileinfo(fi);
	    }
	  else
	    list_add_item_head(&res, fi);
    }
  return res;
}

static struct s_list*	browse_rec(struct s_list* res,
				   const char	*sbrowse,
				   int		len_opath,
				   const char	*path)
{
  struct s_spth		spth;
  DIR			*dir;

  if (!(dir = opendir(path)))
    {
      perror_fail(sbrowse, path);
      return NULL;
    }
  spth.path = path;
  spth.sbrowse = sbrowse;
  res = browse_file(res, &spth, len_opath, dir);
  if (closedir(dir) == -1)
    return NULL;
  return res;
}

struct s_list*	browse(const char		*sbrowse,
		       const char		*path)
{
  struct stat	sb;
  char		*fn;
  struct s_list* res = NULL;

  if (lstat(path, &sb) == -1)
    {
      return NULL;
    }
  fn = strdup(path);
  fn = realloc(fn, (strlen(fn) + 2) * sizeof(char));
  fn = strcat(fn, "/");
  if ((S_ISDIR(sb.st_mode) && !S_ISBLK(sb.st_mode) &&
       !S_ISLNK(sb.st_mode) && !S_ISSOCK(sb.st_mode)))
    {
      free(fn);
      return browse_rec(res, sbrowse, strlen(path), path);
    }
  free(fn);
  return res;
}
