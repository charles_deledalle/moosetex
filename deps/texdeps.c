/***************************************************************************
 * texdeps.c
 * ---------
 *
 *   This file is part of MooseTeX. Compute dependencies between TEX, STY,
 *   BIB, CLS, BST files and images.
 *
 *   Copyright (c) 2013-2015 Charles Deledalle
 *
 * License
 *
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use,
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info".
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability.
 *
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or
 *   data to be ensured and, more generally, to use and operate it in the
 *   same conditions as regards security.
 *
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *
 * Contact
 *
 *   contributor(s): Charles Deledalle
 *
 *   charles (dot) deledalle (at) gmail (dot) com
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "browse.h"

static int strcmp_void(const void *s1, const void*s2)
{
  return strcmp((const char *) s1, (const char *) s2);
}

static int strcasecmp_void(const void *s1, const void*s2)
{
  return strcasecmp((const char *) s1, (const char *) s2);
}

static int file_relfn_cmp_void(const void *s1, const void*s2)
{
  return strcasecmp(((const struct fileinfo *) s1)->relfn, ((const struct fileinfo *) s2)->relfn);
}

static int file_relfn_noext_cmp_void(const void *s1, const void*s2)
{
  return strcasecmp(((const struct fileinfo *) s1)->relfn_noext, ((const struct fileinfo *) s2)->relfn_noext);
}

static void free_fileinfo_void(void *p)
{
  free_fileinfo((struct fileinfo*) p);
}

static void free_noeffect(void *p)
{
  p = p;
}

#define MAXBUF 4096

static struct s_list* used_files_intex(const char* fn)
{
  FILE	*f;
  char	c;
  int	k;
  char	fn_candidate[MAXBUF];
  struct s_list* res = NULL;

  if (!(f = fopen(fn, "r")))
    {
      fprintf(stderr, "Cannot open file %s: %s\n", fn, strerror(errno));
      exit(2);
    }

  k = -1;
  while ((c = fgetc(f)) != EOF)
    {
      if (k == -1)
	if (c == '%')
	  {
	    while (c != '\n' && c != EOF)
	      c = fgetc(f);
	    continue;
	  }
      if (0 <= k && k < MAXBUF)
	{
	  if (c == '=' || c == '#' || c == ':' || c == ';' || c == '\'' || c == '`' ||
	      c == '!' || c == '?' || c == '@' || c == '&' || c == '\\' || c == '%' ||
	      c == '$' || c == '*' || c == '^' ||
	      c == '"' ||
	      c == ' ' || c == '\t' || c == '\n')
	    {
	      k = -1;
	      continue;
	    }
	  if (c == '{' || c == '[')
	    {
	      k = 0;
	      continue;
	    }
	  if (c == '}' || c == ']')
	    {
	      fn_candidate[k] = '\0';
	      if (k > 0)
		list_add_item_sorted(&res, strdup(fn_candidate), &strcmp_void, &free);
	      k = -1;
	    }
	  else
	    if (c == ',')
	      {
		fn_candidate[k] = '\0';
		if (k > 0)
		  list_add_item_sorted(&res, strdup(fn_candidate), &strcmp_void, &free);
		k = 0;
	      }
	    else
	      {
		fn_candidate[k] = c;
		k++;
	      }
	}
      else
	if (c == '{' || c == '[' || c == ',')
	  k = 0;
	else
	  k = -1;
    }
  fclose(f);
  return res;
}


static struct s_list* used_files_insvg(const char* fn, const char* dn)
{
  FILE	*f;
  char	c;
  int	k;
  char	fn_candidate[MAXBUF];
  char	fn_commit[MAXBUF];
  struct s_list* res = NULL;

  if (!(f = fopen(fn, "r")))
    {
      fprintf(stderr, "Cannot open file %s: %s\n", fn, strerror(errno));
      exit(2);
    }

  k = -1;
  while ((c = fgetc(f)) != EOF)
    {
      if (0 <= k && k < MAXBUF)
	{
	  if (c == '=' || c == '#' || c == ':' || c == ';' || c == '\'' || c == '`' ||
	      c == '!' || c == '?' || c == '@' || c == '&' || c == '\\' || c == '%' ||
	      c == '$' || c == '*' || c == '^' || c == '(' || c == ')' ||
	      c == '{' || c == '[' || c == '}' || c == ']' || c == ',' ||
	      c == ' ' || c == '\n')
	    {
	      k = -1;
	      continue;
	    }
	  if (c == '"')
	    {
	      fn_candidate[k] = '\0';
	      fn_commit[0] = '\0';
	      strcat(fn_commit, dn);
	      if (dn[0] != '\0')
		strcat(fn_commit, "/");
	      if (fn_candidate[0] == '.' && fn_candidate[1] == '/')
		strcat(fn_commit, fn_candidate + 2);
	      else
		strcat(fn_commit, fn_candidate);
	      if (k > 0)
		list_add_item_sorted(&res, strdup(fn_commit), &strcmp_void, &free);
	      k = -1;
	    }
	  else
	    {
	      fn_candidate[k] = c;
	      k++;
	    }
	}
      else
	if (c == '"')
	  k = 0;
	else
	  k = -1;
    }
  fclose(f);
  return res;
}

static int isusedby(struct fileinfo* f, const char* s)
{
  return !!list_find_item_eq(f->used_files, s, &strcmp_void);
}

static int searchfor(const char* fn,
		     const char* s)
{
  FILE	*f;
  char	c;
  int	k;
  int   l;

  if (!(f = fopen(fn, "r")))
    {
      fprintf(stderr, "Cannot open file %s: %s\n", fn, strerror(errno));
      exit(2);
    }

  l = strlen(s);
  k = -1;
  while ((c = fgetc(f)) != EOF)
    {
      if (0 <= k && k < l && c == s[k])
	++k;
      else
	if (k == l)
	  break;
	else
	  if (c == s[0])
	    k = 1;
	  else
	    k = -1;
    }
  fclose(f);
  return k == l;
}

static int containillegal(const char *s)
{
  for (; *s != '\0' && *s != ' ' && *s != '#' && *s != ':' && *s != ';'; ++s)
    ;
  return *s == ' ' || *s == '#' || *s == ':' || *s == ';';
}

#define UNUSED ((void*) 0x0)
#define USED ((void*) 0x1)

int main(int argc, char* argv[])
{
  char*		 src;
  char*		 srcimagesdir;
  char*		 format;
  struct s_list* list_filter;
  struct s_list* list;
  struct s_list* list_images;
  struct s_list* lext;
  struct s_list* p;
  struct s_list* p1;
  struct s_list* p2;
  struct fileinfo* fi;
  struct fileinfo* fi1;
  struct fileinfo* fi2;
  int are_there_unused;
  const char* fn_used = ".moosetex/used";
  FILE* f;

  if (argc != 4)
    {
      fprintf(stderr, "Usage: %s srcdir srcimagesdir format\n", argv[0]);
      exit(2);
    }
  src = argv[1];
  srcimagesdir = argv[2];
  format = argv[3];

  if (src[strlen(src)-1] == '/')
    src[strlen(src)-1] = '\0';

  list = browse(argv[0], src);
  list_images = browse(argv[0], srcimagesdir);

  /* Keep .tex .sty and .bib that have no space in filename */
  list_filter = NULL;
  for (p = list; p; p = p->next)
    {
      fi = p->data;
      if (!strcasecmp(fi->ext, ".tex") ||
	  !strcasecmp(fi->ext, ".sty") ||
	  !strcasecmp(fi->ext, ".cls") ||
	  !strcasecmp(fi->ext, ".bst") ||
	  !strcasecmp(fi->ext, ".bib"))
	{
	  if (!containillegal(fi->absfn))
	    list_add_item_sorted(&list_filter, fi, &file_relfn_cmp_void, &free_noeffect);
	  else
	    {
	      fprintf(stderr, "Ignore file \"%s\": Cannot deal with spaces, '#', ';' or ':' in filename\n", fi->relfn);
	      free_fileinfo_void(fi);
	    }
	}
      else
	free_fileinfo_void(fi);
    }
  list_free(&list, &free_noeffect);
  list = list_filter;

  /* Keep images that have no spaces in filename */
  list_filter = NULL;
  for (p = list_images; p; p = p->next)
    {
      fi = p->data;
      if (!containillegal(fi->absfn))
	list_add_item_sorted(&list_filter, fi, &file_relfn_cmp_void, &free_noeffect);
      else
	{
	  fprintf(stderr, "Ignore image \"%s\": Cannot deal with spaces, '#', ';' or ':' in filename\n", fi->relfn);
	  free_fileinfo_void(fi);
	}
    }
  list_free(&list_images, &free_noeffect);
  list_images = list_filter;

  /* Make sure there no two images with same name but different extension */
  for (p1 = list_images; p1 && p1->next; p1 = p1->next)
    {
      p2 = p1->next;
      fi1 = p1->data;
      fi2 = p2->data;
      while (fi1 && fi2 && !file_relfn_noext_cmp_void(fi1, fi2))
	{
	  fprintf(stderr, "Ignore image \"%s\": Image \"%s.%s\" already generated from \"%s\".\n",
		  fi2->relfn, fi1->bn_noext, format, fi1->relfn);
	  p = p2->next;
	  free_fileinfo_void(fi2);
	  free(p2);
	  p1->next = p;

	  p2 = p1->next;
	  fi1 = p1->data;
	  fi2 = p2 ? p2->data : NULL;
	}
    }

  /* Extract extensions used for rules */
  lext = NULL;
  for (p = list; p; p = p->next)
    {
      fi = p->data;
      list_add_item_sorted(&lext, fi->ext, &strcasecmp_void, &free_noeffect);
    }
  for (p = list_images; p; p = p->next)
    {
      fi = p->data;
      list_add_item_sorted(&lext, fi->ext, &strcasecmp_void, &free_noeffect);
    }

  /* Initialized all files to unused */
  for (p = list; p; p = p->next)
    {
      fi = p->data;
      fi->extra = UNUSED;
    }
  for (p = list_images; p; p = p->next)
    {
      fi = p->data;
      fi->extra = UNUSED;
    }

  /* Construct list of used files */
  for (p = list; p; p = p->next)
    {
      fi = p->data;
      if (!strcasecmp(fi->ext, ".tex") ||
	  !strcasecmp(fi->ext, ".sty") ||
	  !strcasecmp(fi->ext, ".cls") ||
	  !strcasecmp(fi->ext, ".bst"))
	fi->used_files = used_files_intex(fi->absfn);
    }
  /* Construct list of used files for images */
  for (p = list_images; p; p = p->next)
    {
      fi = p->data;
      if (!strcasecmp(fi->ext, ".svg"))
	fi->used_files = used_files_insvg(fi->absfn, fi->reldn);
    }

  /* FOR DEBUG */
  /* for (p = list; p; p = p->next) */
  /*   { */
  /*     fi = p->data; */
  /*     for (p1 = fi->used_files; p1; p1 = p1->next) */
  /* 	printf("DEBUG %s |%s|\n", fi->relfn, (char *) p1->data); */
  /*   } */

  /* Create suffixes list */
  if (lext)
    {
      printf(".SUFFIXES: .pdf");
      if (!strcasecmp(format, "eps"))
	printf(" .dvi .ps .eps");
	for (p = lext; p; p = p->next)
	if (strcasecmp((char *) p->data, ""))
	  printf(" %s", (char *) p->data);
      printf("\n\n");
    }
  if (list)
    {
      printf("target:");
      for (p = list; p; p = p->next)
	{
	  fi = p->data;
	  if (!strcasecmp(fi->ext, ".tex"))
	    if (searchfor(fi->absfn, "\\documentclass") &&
		(!strcmp(fi->dn, src) || !strcmp(fi->reldn, src)))
	      {
		if (!strcasecmp(format, "pdf"))
		  printf(" %s.pdf", fi->relfn_noext);
		if (!strcasecmp(format, "eps"))
		  printf(" %s.dvi %s.ps %s.pdf",
			 fi->relfn_noext,
			 fi->relfn_noext,
			 fi->relfn_noext);
	      }
	}
      printf("\n");
      printf("\t@printf \"%%-50.50s Compilation succeed\\n\" \"*\" | $(COLORIZEGREEN);\n");
      printf("\n");
    }
  for (p = list; p; p = p->next)
    {
      fi = p->data;
      if (!strcasecmp(fi->ext, ".tex"))
	if (searchfor(fi->absfn, "\\documentclass") &&
	    (!strcmp(fi->dn, src) || !strcmp(fi->reldn, src)))
	  {
	    fi->extra = USED;
	    if (!strcasecmp(format, "pdf"))
	      printf("%s.pdf: $(TEX_CACHE)/%s.tex\n\n", fi->relfn_noext, fi->relfn_noext);
	    if (!strcasecmp(format, "eps"))
	      printf("%s.dvi: $(TEX_CACHE)/%s.tex\n\n", fi->relfn_noext, fi->relfn_noext);
	  }
    }
  for (p1 = list; p1; p1 = p1->next)
    {
      fi1 = p1->data;
      if (!strcasecmp(fi1->ext, ".tex") || !strcasecmp(fi1->ext, ".sty") ||
	  !strcasecmp(fi1->ext, ".cls") || !strcasecmp(fi1->ext, ".bst"))
	{
	  printf("$(TEX_CACHE)/%s: %s", fi1->relfn, fi1->relfn);
	  for (p2 = list; p2; p2 = p2->next)
	    {
	      fi2 = p2->data;
	      if (!strcmp(fi1->relfn, fi2->relfn))
		continue;
	      if (!strcasecmp(fi2->ext, ".tex") || !strcasecmp(fi2->ext, ".sty") ||
		  !strcasecmp(fi2->ext, ".cls") || !strcasecmp(fi2->ext, ".bst"))
		if (isusedby(fi1, fi2->relfn_noext))
		  {
		    fi2->extra = USED;
		    printf(" $(TEX_CACHE)/%s", fi2->relfn);
		  }
	      if (!strcasecmp(fi2->ext, ".bib"))
		if (isusedby(fi1, fi2->relfn_noext))
		  {
		    fi2->extra = USED;
		    printf(" %s", fi2->relfn);
		  }
	    }
	  for (p2 = list_images; p2; p2 = p2->next)
	    {
	      struct fileinfo * fi2 = p2->data;
	      if (isusedby(fi1, fi2->relfn_noext))
		{
		  fi2->extra = USED;
		  printf(" $(IMAGES_DST_PATH)/%s.%s", fi2->relfn_noext, format);
		}
	    }
	  printf("\n");
	  printf("\t@mkdir -p '$(TEX_CACHE)/%s'\n", fi1->reldn);
	  printf("\t@touch -f '$(TEX_CACHE)/%s'\n\n", fi1->relfn);
	}
    }
  for (p1 = list_images; p1; p1 = p1->next)
    {
      fi1 = p1->data;
      printf("$(IMAGES_DST_PATH)/%s.%s: $(IMAGES_SRC_PATH)/%s", fi1->relfn_noext, format, fi1->relfn);
      if (!strcasecmp(fi1->ext, ".svg"))
	for (p2 = list_images; p2; p2 = p2->next)
	  {
	    fi2 = p2->data;
	    if (!strcmp(fi1->relfn, fi2->relfn))
	      continue;
	    if (isusedby(fi1, fi2->relfn))
	      {
		if (!fi2->extra)
		  fi2->extra = fi1->extra;
		printf(" $(IMAGES_SRC_PATH)/%s", fi2->relfn);
	      }
	  }
      printf("\n");
      printf("\t@echo \"%-50.50s Generate image\"\n", fi1->relfn);
      printf("\t@mkdir -p '$(IMAGES_DST_PATH)/%s'\n", fi1->reldn);
      printf("\t@(($(MT_SCRIPTS_PATH)/mt_convert --use-directives='$(IMAGES_DIRECTIVES)' --prebuiltdir='$(IMAGES_PREDST_PATH)' \\\n");
      printf("\t                                '$(IMAGES_SRC_PATH)/%s' '$(IMAGES_DST_PATH)/%s.%s' \\\n", fi1->relfn, fi1->relfn_noext, format);
      printf("\t    | $(SEDUNBUFFERED) 's|\\(.*\\)|%-50.50s \\1|' | ($(COLORIZEYELLOW); true)) 3>&1 1>&2- 2>&3- \\\n", fi1->relfn);
      printf("\t    | $(SEDUNBUFFERED) 's|\\(.*\\)|%-50.50s \\1|' | ($(COLORIZERED);  true)) 3>&1 1>&2- 2>&3-\n", fi1->relfn);
      printf("\n");
    }
  if ((f = fopen(fn_used, "r")))
    {
      fclose(f);
      for (p = list; p; p = p->next)
	{
	  fi = p->data;
	  if (searchfor(fn_used, fi->relfn))
	    fi->extra = USED;
	}
      for (p = list_images; p; p = p->next)
	{
	  fi = p->data;
	  if (searchfor(fn_used, fi->relfn))
	    fi->extra = USED;
	}
    }
  for (p = list; p; p = p->next)
    {
      fi = p->data;
      if (!fi->extra && (!strcasecmp(fi->ext, ".tex") || !strcasecmp(fi->ext, ".sty") ||
			 !strcasecmp(fi->ext, ".cls") || !strcasecmp(fi->ext, ".bst") ||
			 !strcasecmp(fi->ext, ".bib")))
	fprintf(stderr, "Unused file %s\n", fi->relfn);
    }
  printf("vacuum:\n");
  printf("\t@printf 'Vacuum cleaning\\n' | $(COLORIZERED);\n");
  printf("\t@printf '===============\\n\\n' | $(COLORIZERED);\n");
  are_there_unused = 0;
  for (p = list_images; p; p = p->next)
    {
      fi = p->data;
      if (!fi->extra)
	{
	  printf("\t@echo Move \"$(IMAGES_SRC_PATH)/%s\" to \"$(IMAGES_TRASH_PATH)\" ;\n", fi->relfn);
	  printf("\t@mkdir -p \"$(IMAGES_TRASH_PATH)/%s\" ;\n", fi->reldn);
	  printf("\t@mv -f \"$(IMAGES_SRC_PATH)/%s\" \"$(IMAGES_TRASH_PATH)/%s\" ;\n", fi->relfn, fi->reldn);
	  fprintf(stderr, "Unused image %s\n", fi->relfn);
	  are_there_unused = 1;
	}
    }
  printf("\t@printf 'Vacuuming success\\n\\n' | $(COLORIZEGREEN);\n");
  if (are_there_unused)
    fprintf(stderr, "Type 'moosetex vacuum' to move the useless images to the trash directory\n");

  list_free(&list_images, &free_fileinfo_void);
  list_free(&list, &free_fileinfo_void);
  list_free(&lext, &free_noeffect);

  return EXIT_SUCCESS;
}
