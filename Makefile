############################################################################
# Makefile
# --------
#
#   This file is part of MooseTeX. Compile and intall MooseTeX.
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

MAKEFILE_RULES=Makefile.rules

-include $(MAKEFILE_RULES)

.PHONY: doc

all:
	make -C colorize all
	make -C deps all
	find scripts/ -regex '[^.]*' -exec chmod 755 {} \;
	make doc

clean:
	which moosetex >/dev/null && moosetex --default -C doc clean ; true
	find . -name '*~' -exec rm -f {} \;
	make -C colorize clean
	make -C deps clean

distclean:
	which moosetex >/dev/null && moosetex --default -C doc distclean ; true
	find . -name '*~' -exec rm -f {} \;
	make -C colorize distclean
	make -C deps distclean
	rm -rf doc/images
	rm -f $(MAKEFILE_RULES)

doc:
	if ! [ -f $(HOME)/.moosetex/conf ] ; then	\
		./moosetex --default -C doc;		\
		rm -f ~/.moosetex/conf;			\
	else						\
		./moosetex --default -C doc;		\
	fi

install:
	make -C colorize install
	make -C deps install
	mkdir -p $(DESTDIR)/$(PREFIX)/bin/
	mkdir -p $(DESTDIR)/$(PREFIX)/share/moosetex/
	mkdir -p $(DESTDIR)/$(PREFIX)/share/moosetex/scripts/
	mkdir -p $(DESTDIR)/$(PREFIX)/share/doc/moosetex/
	install -m 0755 moosetex $(DESTDIR)/$(PREFIX)/bin
	install -m 0755 Makefile.rules $(DESTDIR)/$(PREFIX)/share/moosetex/conf
	install -m 0755 scripts/* $(DESTDIR)/$(PREFIX)/share/moosetex/scripts
	install -m 0755 doc/doc.pdf $(DESTDIR)/$(PREFIX)/share/doc/moosetex/
	bc="`which bash`" ; bc="`dirname $$bc`" ;								\
	if [ -d $$bc/../etc/bash_completion.d/ ] && [ -w $$bc/../etc/bash_completion.d/ ] ; then	\
		install -m 0755 others/bash_completion.sh $$bc/../etc/bash_completion.d/moosetex;	\
	else												\
	  	install -m 0755 others/bash_completion.sh $(DESTDIR)/$(PREFIX)/share/moosetex/;			\
	fi;												\
	if [ -d $(DESTDIR)/$(PREFIX)/share/pixmaps/ ] && [ -w $(DESTDIR)/$(PREFIX)/share/pixmaps/ ] ; then			\
		mkdir -p $(DESTDIR)/$(PREFIX)/share/pixmaps/;							\
		install -m 0755 media/icon.svg $(DESTDIR)/$(PREFIX)/share/pixmaps/moosetex.svg;			\
		install -m 0755 media/icon.png $(DESTDIR)/$(PREFIX)/share/pixmaps/moosetex.png;			\
	fi


uninstall:
	rm -f $(DESTDIR)/$(PREFIX)/bin/moosetex
	rm -rf $(DESTDIR)/$(PREFIX)/share/moosetex/
	bc="`which bash`" ; bc="`dirname $$bc`" ;								\
	if [ -d $$bc/../etc/bash_completion.d/ ] && [ -w $$bc/../etc/bash_completion.d/ ] ; then	\
		rm -f $$bc/../etc/bash_completion.d/moosetex;						\
	fi
	if [ -d $(PREFRIX)/share/pixmaps/ ] && [ -w $(DESTDIR)/$(PREFIX)/share/pixmaps/ ] ; then			\
		rm -f $(DESTDIR)/$(PREFIX)/share/pixmaps/moosetex.svg;						\
		rm -f $(DESTDIR)/$(PREFIX)/share/pixmaps/moosetex.png;						\
	fi

purge: uninstall
	rm -rf $(HOME)/.moosetex
