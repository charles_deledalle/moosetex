/***************************************************************************
 * colorize.c
 * ----------
 *
 *   This file is part of MooseTeX. Read standard input and print the result
 *   to standard output delimited by terminal like color caracters.
 *
 *   Copyright (c) 2013-2015 Charles Deledalle
 *
 * License
 *
 *   This software is governed by the CeCILL license under French law and
 *   abiding by the rules of distribution of free software. You can use,
 *   modify and/ or redistribute the software under the terms of the CeCILL
 *   license as circulated by CEA, CNRS and INRIA at the following URL
 *   "http://www.cecill.info".
 *
 *   As a counterpart to the access to the source code and rights to copy,
 *   modify and redistribute granted by the license, users are provided only
 *   with a limited warranty and the software's author, the holder of the
 *   economic rights, and the successive licensors have only limited
 *   liability.
 *
 *   In this respect, the user's attention is drawn to the risks associated
 *   with loading, using, modifying and/or developing or reproducing the
 *   software by the user in light of its specific status of free software,
 *   that may mean that it is complicated to manipulate, and that also
 *   therefore means that it is reserved for developers and experienced
 *   professionals having in-depth computer knowledge. Users are therefore
 *   encouraged to load and test the software's suitability as regards their
 *   requirements in conditions enabling the security of their systems and/or
 *   data to be ensured and, more generally, to use and operate it in the
 *   same conditions as regards security.
 *
 *   The fact that you are presently reading this means that you have had
 *   knowledge of the CeCILL license and that you accept its terms.
 *
 * Contact
 *
 *   contributor(s): Charles Deledalle
 *
 *   charles (dot) deledalle (at) gmail (dot) com
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int
main(int argc, char* argv[])
{
  FILE* f = stdin;
  char c;
  int newline = 1;

  if (argc != 3)
    {
      fprintf(stderr, "Usage: %s type color\n", argv[0]);
      exit(2);
    }

  if (getenv("TERM") && strcmp(getenv("TERM"), "dumb"))
    while ((c = fgetc(f)) != EOF)
      {
	if (newline)
	  {
	    fputc(033, stdout);
	    fputc('[', stdout);
	    fputs(argv[1], stdout);
	    fputc(';', stdout);
	    fputs(argv[2], stdout);
	    fputc('m', stdout);
	    fputc(033, stdout);
	    fputc('[', stdout);
	    fputc('K', stdout);
	    newline = 0;
	  }
	if (c == '\n')
	  {
	    fputc(033, stdout);
	    fputc('[', stdout);
	    fputc('m', stdout);
	    fputc(033, stdout);
	    fputc('[', stdout);
	    fputc('K', stdout);
	    fputc('\n', stdout);
	    fflush(stdout);
	    newline = 1;
	  }
	else
	  fputc(c, stdout);
      }
  else
    while ((c = fgetc(f)) != EOF)
      {
	fputc(c, stdout);
	if (c == '\n')
	  fflush(stdout);
      }
  fflush(stdout);
  return EXIT_SUCCESS;
}

