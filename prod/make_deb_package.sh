#!/bin/bash

############################################################################
# make_deb_package
# ----------------
#
#   This file is part of MooseTeX. Create a deb package for MooseTeX.
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

if [ $# -gt 0 ] ; then
    echo Usage: $0
    exit 1
fi
echo "This script is intended for production only."
echo "Is is supposed to be run on 64 bits Ubuntu Linux."
echo "Do NOT run it without knowing what it is."
echo "Run this script WITHOUT super-user privileges."
echo "But super-user privileges will be asqued you later on."
echo "A directory $HOME/pbuilder will be created."
echo "This script will take some time..."
echo
echo -n "Proceed anyway? [yN] "
read answer
echo
if [ "$answer" != "y" ] && [ "$answer" != "Y" ] ; then
    exit 0;
fi

rm -f ~/pbuilder/*/moosetex*.deb

# Install MooseTeX
cd ..
./configure
make
sudo make install
cd -


# Get version
source ../scripts/mt_utils
version=`moosetex --version | extract_version`
echo
echo -n "MooseTeX version is $version, right? [yN] "
read answer
echo
if [ "$answer" != "y" ] && [ "$answer" != "Y" ] ; then
    exit 0;
fi


tmpdir=/tmp/moosetex-$version
tmptar=/tmp/moosetex-$version.tar.gz
tmpdoc=/tmp/moosetex-$version-doc.pdf
tmppac=~/moosetex-$version-package

# Copy documentation
cp ../doc/doc.pdf $tmpdoc

# Make Tarball and install Moosetex
cp -r .. $tmpdir
make -C $tmpdir distclean
cd $tmpdir/..
tar --exclude=.svn --exclude=.git -zcf $tmptar moosetex-$version
cd -

############################
#### Create deb package ####
############################

rm -rf $tmppac
mkdir -p $tmppac/moosetex
cd $tmppac/moosetex
cp $tmptar moosetex_$version.orig.tar.gz
tar -zxf moosetex_$version.orig.tar.gz
cd -
cd $tmppac/moosetex/moosetex-$version
DHLIB="/usr/share/debhelper/dh_make/"
DEBFULLNAME="Charles Deledalle"
email="charles.deledalle@gmail.com"
year=2015

### dh_make ###
echo "Run: dh_make"

dh_make -e $email <<EOF
s

EOF
cd -
cd $tmppac/moosetex/moosetex-$version/debian
rm -rf *ex *EX README*
ls

. /etc/lsb-release

echo "-> ChangeLog"
sed -i "s ($version-1) ($version) " changelog
sed -i "s unstable $DISTRIB_CODENAME " changelog
sed -i "s/Initial release.*/Initial release/" changelog

echo "-> Copyright"
cat <<EOF > copyright
Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: moosetex
Source: <http://www.math.u-bordeaux1.fr/~cdeledal/moosetex>

Files: *
Copyright: $year $DEBFULLNAME <$email>
License: CeCILL
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software. You can use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty and the software's author, the holder of the
 economic rights, and the successive licensors have only limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading, using, modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate, and that also
 therefore means that it is reserved for developers and experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
x requirements in conditions enabling the security of their systems and/or
 data to be ensured and, more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
EOF

echo "-> Control"
sed -i "s/Section: unknown/Section: optional/" control
sed -i "s/\(Build-Depends: .*\)/\1, moreutils, gcc, make/" control
sed -i "s/\(Depends: .*\)/\1, coreutils, moreutils, util-linux, wget, unzip, make, texlive-latex-base, texlive-latex-extra, texlive-latex-recommended, texlive-extra-utils, ghostscript, imagemagick, xvfb, poppler-utils, bc, inkscape, epstool/" control
sed -i "s/collab-maint/deledalle/" control
sed -i "s%<insert the upstream URL, if relevant>%http://www.math.u-bordeaux1.fr/~cdeledal/moosetex%" control
sed -i "s/Description: <insert up to 60 chars description>/Description: The easy way to use LaTeX/" control
sed -i "s/<insert long description, indented with spaces>/MooseTeX is open-source software distributed under CeCILL license for UNIX-like systems (such as Linux and MacOS-X). It makes the most of your LaTeX project by helping you generate high quality documents in PDF format, and if required DVI and PS formats. MooseTeX aims to be used for any kind of projects such as articles, letters, reports, theses, presentations or posters./" control

echo "-> Include-Binaries"
cat <<EOF > source/include-binaries
debian/moosetex/usr/share/doc/moosetex/doc.pdf
debian/moosetex/usr/share/moosetex/scripts/mt_texdeps
debian/moosetex/usr/share/moosetex/scripts/mt_grayimage.pdf
debian/moosetex/usr/share/moosetex/scripts/mt_colorize
EOF

### Building package ###
echo ">>>>>>>>>>>>>>>>> Run: debuild"
cd -
cd $tmppac/moosetex/moosetex-$version/
debuild -S -sa --lintian-opts -i
cd -
cd $tmppac/moosetex/
echo "COMPONENTS=\"main restricted universe multiverse\"" | tee -a ~/.pbuilderrc

### Pbuilder-dist ###
echo ">>>>>>>>>>>>>>>>> Run: pbbuilder-dist"
if ! [ -f ~/pbuilder/$DISTRIB_CODENAME-i386-base.tgz ] ; then
    pbuilder-dist $DISTRIB_CODENAME i386 create
else
    echo $DISTRIB_CODENAME-i386: already created
fi
pbuilder-dist $DISTRIB_CODENAME i386 *.dsc

if ! [ -f ~/pbuilder/$DISTRIB_CODENAME-base.tgz ] ; then
    pbuilder-dist $DISTRIB_CODENAME amd64 create
else
    echo $DISTRIB_CODENAME-amd64: already createded
fi
pbuilder-dist $DISTRIB_CODENAME amd64 *.dsc

# Get back generated files
mv -f $tmpdoc .
mv -f $tmptar .
cp -f ~/pbuilder/${DISTRIB_CODENAME}-i386_result/*.deb .
cp -f ~/pbuilder/${DISTRIB_CODENAME}_result/*.deb .
rm -rf $tmpdir
