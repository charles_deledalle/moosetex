#!/bin/bash

############################################################################
# change_copyright
# ----------------
#
#   This file is part of MooseTeX. Change the copyright of all files of
#   the project.
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

if [ $# -ne 2 ] ; then
    echo "Usage: " `basename $0` "years name"
    echo
    echo "  years (ex: '2013-2014')"
    echo "  name (ex: 'Foo Bar')"
    exit 1
    echo
fi
years=$1
name=$2

echo "Do you want to change copyright to 'Copyright (c) $years $name'? [yN] " # ¼
read answer
if [ "$answer" != "y" ] && [ "$answer" != "Y" ] ; then
    exit 0;
fi
while read file ; do
    if grep -q "Copyright (c)[^¼]*$" "$file" ; then # ¼
	echo "Update $file"
	sed -i "s/Copyright (c)[^¼]*$/Copyright (c) $years $name/" "$file" # ¼
    fi
done < <(find .. -not -path '*/\.*')
