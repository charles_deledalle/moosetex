#!/bin/bash

# Make temporary directory
cleanup()
{
    if [ "$tmpdir" != "" ] ; then
	rm -rf "$tmpdir"
    fi
}

bn=$(basename $0)
case "`uname`" in
    Darwin)
	declare -r tmpdir="`mktemp -d -t $bn`";
	;;
    *)
	declare -r tmpdir="`mktemp -d -t ${bn}_XXXXXX`";
	;;
esac

trap "cleanup; exit;" EXIT INT TERM HUP CHLD SIGCHLD SIGINT SIGTERM SIGHUP

# Display functions
function green
{
    echo $* | GREP_COLOR="1;32" grep --color ".*"
}

function red
{
    echo $* | GREP_COLOR="1;31" grep --color ".*"
}

function white
{
    echo $* | GREP_COLOR="1;29" grep --color ".*"
}

# Check image conversion
while read line ; do
    mkdir -p "$tmpdir/images"
    k=`echo "$line" | cut -d' ' -f 1`
    if [ "$k" == "0" ] ; then
	echo
	name=`echo "$line" | cut -d' ' -f 2-`
	white "$name"
	echo
	continue
    fi
    compile=`echo "$line" | cut -d'|' -f 1 | cut -d' ' -f 2`
    warning=`echo "$line" | cut -d'|' -f 1 | cut -d' ' -f 3`
    error=`echo "$line" | cut -d'|' -f 1 | cut -d' ' -f 4`
    fname=`echo "$line" | cut -d'|' -f 1 | cut -d' ' -f 5`
    options=`echo "$line" | cut -d'|' -f 2`

    echo -n "$k. "
    echo "$line ($fname)" | cut -d'|' -f 1 | cut -d' ' -f 6-

    if [ "$compile" == "1" ] ; then
	printf "\t%-50.50s" "Should compile "
    else
	printf "\t%-50.50s" "Should NOT compile "
    fi
    if moosetex convert $options "images/$fname" "$tmpdir/images/$fname.pdf" \
	2> "$tmpdir/error.txt" > "$tmpdir/output.txt" ; then
	([ "$compile" == "1" ] && green "[OK]") || red "[KO]"
    else
	([ "$compile" == "0" ] && green "[OK]") || red "[KO]"
    fi
    grep -v -e '^Starting' -e '^Stopping' -e "^The image .* by a gray one.$" "$tmpdir/output.txt" \
	> "$tmpdir/warning.txt"

    if [ "$warning" == "1" ] ; then
	printf "\t%-50.50s" "Should produce warning "
    else
	printf "\t%-50.50s" "Should NOT produce warning "
    fi
    if grep -q '.' "$tmpdir/warning.txt" ; then
	([ "$warning" == "1" ] && green -n "[OK]") || red -n "[KO]"
	while read errline ; do
	    printf "\t\t%s\n" "$errline"
	done < <(cat "$tmpdir/warning.txt")
    else
	([ "$warning" == "0" ] && green "[OK]") || red "[KO]\t"
    fi

    if [ "$error" == "1" ] ; then
	printf "\t%-50.50s" "Should produce error "
    else
	printf "\t%-50.50s" "Should NOT produce error "
    fi
    if grep -q '.' "$tmpdir/error.txt" ; then
	([ "$error" == "1" ] && green -n "[OK]") || red -n "[KO]"
	while read errline ; do
	    printf "\t\t%s\n" "$errline"
	done < <(cat "$tmpdir/error.txt")
    else
	([ "$error" == "0" ] && green "[OK]") || red "[KO]\t"
    fi

done < <(cat "images/desc.txt")
