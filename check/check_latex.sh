#!/bin/bash

# Make temporary directory
cleanup()
{
    if [ "$tmpdir" != "" ] ; then
	rm -rf "$tmpdir"
    fi
}

bn=$(basename $0)
case "`uname`" in
    Darwin)
	declare -r tmpdir="`mktemp -d -t $bn`";
	;;
    *)
	declare -r tmpdir="`mktemp -d -t ${bn}_XXXXXX`";
	;;
esac

trap "cleanup; exit;" EXIT INT TERM HUP CHLD SIGCHLD SIGINT SIGTERM SIGHUP

# Display functions
function green
{
    echo $* | GREP_COLOR="1;32" grep --color ".*"
}

function red
{
    echo $* | GREP_COLOR="1;31" grep --color ".*"
}

function white
{
    echo $* | GREP_COLOR="1;29" grep --color ".*"
}

if [ $# -gt 0 ] ; then
    options="`ls -d latex/$1/`"
else
    options="`ls -d latex/*/`"
fi

# Check latex compilation
for dir in $options ; do
    mkdir -p "$tmpdir/$dir"

    if ! [ -f "$dir/desc.txt" ] ; then
	continue
    fi
    while read line ; do
	k=`echo "$line" | cut -d' ' -f 1`
	if [ "$k" == "0" ] ; then
	    echo
	    name=`echo "$line" | cut -d' ' -f 2-`
	    white "$name ($dir)"
	    echo
	    continue
	fi
	compile=`echo "$line" | cut -d' ' -f 2`
	nbrunlatex=`echo "$line" | cut -d' ' -f 3`
	nbrunbibtex=`echo "$line" | cut -d' ' -f 4`
	nbrunmakeindex=`echo "$line" | cut -d' ' -f 5`
	echo -n "$k. "
	echo "$line" | cut -d' ' -f 6-
	for filein in `find "$dir" -name "*$k.*"` ; do
	    fileout=`echo "$filein" | sed "s/$k\././"`
	    mkdir -p "$(dirname "$tmpdir/$fileout")"
	    [ "`uname`" == "Darwin" ] && sleep 1
	    cp "$filein" "$tmpdir/$fileout"
	done
	if [ "$k" == "1" ] ; then
	    moosetex -C "$tmpdir/$dir" configure --default > /dev/null
	fi
	if [ "$compile" == "1" ] ; then
	    printf "\t%-50.50s" "Should compile "
	else
	    printf "\t%-50.50s" "Should NOT compile "
	fi
	if moosetex -C "$tmpdir/$dir" >  "$tmpdir/output.txt" ; then
	    ([ "$compile" == "1" ] && green "[OK]") || red "[KO]"
	else
	    ([ "$compile" == "0" ] && green "[OK]") || red "[KO]"
	fi
	sed -n 's/.*=> \(.*\)/\1/p' "$tmpdir/output.txt" > "$tmpdir/output.2.txt"

	if grep -q "^latex [[:digit:]].*$" "$tmpdir/output.2.txt" > /dev/null ; then
	    nbrunlatex_bis=`sed -n "s/^latex \([[:digit:]]*\).*$/\1/p" "$tmpdir/output.2.txt"`
	else
	    nbrunlatex_bis=0
	fi
	printf "\t%-50.50s" "Should run LaTeX $nbrunlatex time(s) "
	([ $nbrunlatex_bis -eq $nbrunlatex ] && green "[OK]") || red "[KO]"

	if grep -q "^bibtex [[:digit:]].*$" "$tmpdir/output.2.txt" > /dev/null ; then
	    nbrunbibtex_bis=`sed -n "s/^bibtex \([[:digit:]]*\).*$/\1/p" "$tmpdir/output.2.txt"`
	else
	    nbrunbibtex_bis=0
	fi
	printf "\t%-50.50s" "Should run Bibtex $nbrunbibtex time(s) "
	([ $nbrunbibtex_bis -eq $nbrunbibtex ] && green "[OK]") || red "[KO]"

	if grep -q "^makeindex [[:digit:]].*$" "$tmpdir/output.2.txt" > /dev/null ; then
	    nbrunmakeindex_bis=`sed -n "s/^makeindex \([[:digit:]]*\).*$/\1/p" "$tmpdir/output.2.txt"`
	else
	    nbrunmakeindex_bis=0
	fi
	printf "\t%-50.50s" "Should run Makeindex $nbrunmakeindex time(s) "
	([ $nbrunmakeindex_bis -eq $nbrunmakeindex ] && green "[OK]") || red "[KO]"

	#[ $k -eq 1 ] && cat "$tmpdir/output.txt"
	#[ $k -eq 2 ] && cat "$tmpdir/output.txt"

    done < <(cat "$dir/desc.txt")
    moosetex -C "$tmpdir/$dir" purge > /dev/null
done
