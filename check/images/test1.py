import matplotlib.pyplot as plt
import numpy as np

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

x, y = np.random.random((2,100))
plt.plot(x, y, ',')
xmin, xmax = 0, 1
ymin, ymax = 0, 1
plt.xlim(xmin, xmax)
plt.ylim(ymin, ymax)
plt.title(r'Python Example 1',  fontsize=12);
