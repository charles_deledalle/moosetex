############################################################################
# bash_completion.sh
# ------------------
#
#   This file is part of MooseTeX. Define fr bash the completions for
#   options of the command "moosetex".
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

_moosetexconvert()
{
    local cur prev prev2 opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    if [[ "$cur" == -* ]]; then
	opts="--size --margin --use-directives --matlab-print-to --prebuiltdir --batch-input-to --help $files"
	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
	_filedir
    fi
}

_moosetexfile()
{
    local cur prev prev2 opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    opts=`ls *.tex 2> /dev/null`
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
}

_moosetex()
{
    local cur prev prev2 opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    for i in `seq $((COMP_CWORD-1))` ; do
	case "${COMP_WORDS[i]}" in
	    "convert")
		_moosetexconvert
		return 0
		;;
	    *.tex)
		_moosetexfile
		return 0
		;;
	    [^-]*)
		if [ "${COMP_WORDS[i-1]}" != "-C" ] && [ "${COMP_WORDS[i-1]}" != "-f" ] && [ "${COMP_WORDS[i-1]}" != "--force" ] ; then
		    return 0
		fi
		;;
	esac
    done
    if [[ "$cur" == -* ]]; then
	opts="-C --nocolor --default --force -f"
    else
	files=`ls *.tex 2> /dev/null`
	opts="configure vacuum clean distclean purge convert update version help $files"
	case "${prev}" in
	    -C)
		_filedir -d
		return 0
		;;
	    --force | -f)
		_filedir
		return 0
		;;
            *)
		;;
	esac
    fi
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
}
complete -F _moosetex moosetex
