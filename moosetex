#!/bin/bash

############################################################################
# moosetex
# --------
#
#   This file is part of MooseTeX. Provide the main interface between the
#   user and the MooseTeX functions.
#
#   Exit 0: success
#   Exit 1: invalid arguments
#   Exit 2: Compilation error occurs
#   Exit 3: Fata error
#
#   Copyright (c) 2013-2015 Charles Deledalle
#
# License
#
#   This software is governed by the CeCILL license under French law and
#   abiding by the rules of distribution of free software. You can use,
#   modify and/ or redistribute the software under the terms of the CeCILL
#   license as circulated by CEA, CNRS and INRIA at the following URL
#   "http://www.cecill.info".
#
#   As a counterpart to the access to the source code and rights to copy,
#   modify and redistribute granted by the license, users are provided only
#   with a limited warranty and the software's author, the holder of the
#   economic rights, and the successive licensors have only limited
#   liability.
#
#   In this respect, the user's attention is drawn to the risks associated
#   with loading, using, modifying and/or developing or reproducing the
#   software by the user in light of its specific status of free software,
#   that may mean that it is complicated to manipulate, and that also
#   therefore means that it is reserved for developers and experienced
#   professionals having in-depth computer knowledge. Users are therefore
#   encouraged to load and test the software's suitability as regards their
#   requirements in conditions enabling the security of their systems and/or
#   data to be ensured and, more generally, to use and operate it in the
#   same conditions as regards security.
#
#   The fact that you are presently reading this means that you have had
#   knowledge of the CeCILL license and that you accept its terms.
#
# Contact
#
#   contributor(s): Charles Deledalle
#
#   charles (dot) deledalle (at) gmail (dot) com
#
############################################################################

version=1.23
year=2015

# does the same as "readlink -f" which is not supported on MacOS X Lion
function readlinkrec {
    targetfile=$1
    curdir="`pwd`"
    case "`basename "$targetfile"`" in
	".." | "." | "/")
	    cd "$targetfile";
	    targetfile=""
	    ;;
	*)
	    cd "`dirname "$targetfile"`"
	    targetfile="`basename "$targetfile"`"
	    ;;
    esac
    while [ -L "$targetfile" ]
    do
	targetfile="`readlink "$targetfile"`"
	cd "`dirname "$targetfile"`"
	targetfile="`basename "$targetfile"`"
    done
    physdir="`pwd -P`"
    if [ "$physdir" == "/" ] ; then
	echo "/$targetfile"
    else
	echo "$physdir/$targetfile"
    fi
    cd "$curdir"
}

# Ask question and return 0 if the answer is correct, 1 otherwise.
# The answer is places in the global variable answer.
export answer="";
function question {
    quest=$1
    answers=$2
    default=$3

    if [ "$default" ] && [ "$USEDEFAULT" == "1" ]; then
        echo $quest -- $default
	answer=$default;
	return 0;
    fi
    while true ; do
	printf "$quest"
	if [ "$answers" != "" ] ; then
	    printf " ($answers)";
	fi
	if [ "$default" != "" ] ; then
	    printf " [$default]: ";
	else
	    printf ": ";
	fi
	read answer;
	if [ "$answer" == "" ] ; then
	    if [ "$default" == "" ] ; then
		continue;
	    else
		answer=$default;
	    fi
	fi
	if (echo $answers | grep "^$answer/" > /dev/null) ||	\
	    (echo $answers | grep "/$answer/" > /dev/null) ||	\
	    (echo $answers | grep "$answer\$" > /dev/null) ||	\
	    [ "$answers" == "" ]; then
	    return 0;
	fi
    done
    return 1;
}

function question_readabledir {
    cd "$WORK_PATH"
    question "$1" "$2" "$3";
    answer="`readlinkrec $answer`";
    while ! [ -d $answer ]; do
	question "$1" "$2" "$3";
	answer="`readlinkrec $answer`";
    done
    cd - > /dev/null
}

function question_readablefile {
    cd "$WORK_PATH"
    question "$1" "$2" "$3";
    answer="`readlinkrec $answer`";
    while ! [ -f $answer ]; do
	question "$1" "$2" "$3";
	answer="`readlinkrec $answer`";
    done
    cd - > /dev/null
}

function question_writabledir {
    cd "$WORK_PATH"
    question "$1" "$2" "$3";
    answer="`readlinkrec "$answer"`";
    exist=1;
    if ! [ -d "$answer" ] ; then
	exist=0;
	mkdir -p "$answer" 2> /dev/null;
    fi
    while ! [ -d "$answer" ]; do
	question "$1" "$2" "$3";
	answer="`readlinkrec "$answer"`";
	exist=1;
	if ! [ -f "$answer" ] ; then
	    exist=0;
	    mkdir -p "$answer" 2> /dev/null;
	fi
    done
    if [ $exist == 0 ] ; then
	rm -rf "$answer";
    fi
    answer="`echo "$answer" | sed "s%${WORK_PATH}/%%"`"
    cd - > /dev/null
}

function question_writablefile {
    cd "$WORK_PATH"
    question "$1" "$2" "$3";
    answer="`readlinkrec "$answer"`";
    exist=1;
    if ! [ -f "$answer" ] ; then
	exist=0;
	touch "$answer" 2> /dev/null;
    fi
    while ! [ -w "$answer" ]; do
	question "$1" "$2" "$3";
	answer="`readlinkrec "$answer"`";
	exist=1;
	if ! [ -f "$answer" ] ; then
	    exist=0;
	    touch "$answer" 2> /dev/null;
	fi
    done
    if [ $exist == 0 ] ; then
	rm -f "$answer";
    fi
    answer="`echo "$answer" | sed "s $WORK_PATH/  "`"
    cd - > /deb/null
}

function yesnoto01 {
    case "$1" in
	"y")
	    echo 1;
	    ;;
	"n")
	    echo 0;
	    ;;
    esac
    return 1;
}

function supress_spaces {
    echo "$1" | sed 's/[()[:blank:]]/_/g'
}

function usage {
    printf "Usage: $(basename $0) [options] [subcommand] [file1.tex ... fileN.tex]\n";
    printf "\n";
    printf "Generate all LaTeX documents placed in the current directory\n"
    printf "(or only file1.tex ... fileN.tex if specified).\n";
    printf "\n";
    printf "Available commands:\n";
    printf "> moosetex             Run generation.\n";
    printf "> moosetex configure   (Re)configure.\n";
    printf "> moosetex vacuum      Move the unused images to the trash directory.\n";
    printf "> moosetex clean       Remove intermediate files.\n";
    printf "> moosetex distclean   Remove all generated files.\n";
    printf "> moosetex purge       Remove all generated files and configuration files.\n";
    printf "> moosetex convert [options] input-file output-file\n";
    printf "                       Convert one or several images to eps or pdf format.\n";
    printf "                       For more information type \"moosetex convert --help\".\n";
    printf "> moosetex update      Update list of available software.\n";
    printf "> moosetex version     Print the version number and exit.\n";
    printf "> moosetex help        Print this message and exit.\n";
    printf "\n";
    printf "Available options:\n";
    printf "  --backward|-b        Change before doing anything to the first backward\n"
    printf "                       directory being a MooseTex project.\n"
    printf "                       Current directory is selected if none are found.\n"
    printf "  -C DIRECTORY         Change of directory before doing anything.\n"
    printf "  --default|-d         Use default answer whatever the question.\n"
    printf "  --force|-f FILE      Force regenerating FILE (if used).\n"
    printf "  --nocolor            Suppress color output.\n"
    printf "\n";
    printf "Enjoy!\n";
    printf "\n";
}

export DISPLAYLOGO_ALREADY=0
function displaylogo {

    if [ "$DISPLAYLOGO_ALREADY" == "0" ] ; then
	printf "\n";
	printf " /\/\__/\_/\      /\_/\__/\/\         Welcome to MooseTeX $version ($year)\n"
	printf " \          \____/          /         author Charles Deledalle\n"
	printf "  '----_____       ____----'\n"
	printf "            | 0 0  \_\n"
	printf "            |                         The easy way to use LaTeX\n"
	printf "          _/     /\\n"
	printf "         /o)  (o/l \_                 CeCILL license\n"
	printf "         \=====//\n"
	printf "\n";
	DISPLAYLOGO_ALREADY=1
    fi
}

##################### BEGIN ###################@@

### Define project variables
WORK_PATH="`pwd`"
WORK_PATH="`readlinkrec "$WORK_PATH"`"

# Directory where moosetex is installed
MT_PATH="`dirname $0`"
MT_PATH="`readlinkrec $MT_PATH`"

# Directory where internal scripts are installed
if [ -f $MT_PATH/scripts/mt_makefile ] ; then
    MT_SHARE_PATH=$MT_PATH
    MT_SHAREDOC_PATH=$MT_PATH/doc
else
    if [ -f $MT_PATH/../share/moosetex/scripts/mt_makefile ] ; then
	MT_SHARE_PATH=$MT_PATH/../share/moosetex
	MT_SHAREDOC_PATH=$MT_PATH/../share/doc/moosetex
    else
	echo "Fatal error: cannot locate internal scripts ($(basename $0):$LINENO)"
	exit 3
    fi
fi
MT_SHARE_PATH="`readlinkrec $MT_SHARE_PATH`"

# Directory for the sub-scripts
MT_SCRIPTS_PATH=$MT_SHARE_PATH/scripts

# Source temporary files manager
source "$MT_SCRIPTS_PATH/mt_maketmpenv"

# Locate and source file of moosetex' installation
if [ -f $MT_SHARE_PATH/conf ] ; then
    MT_CONF_BASE=$MT_SHARE_PATH/conf
else
    if [ -f $MT_PATH/Makefile.rules ] ; then
	MT_CONF_BASE=$MT_PATH/Makefile.rules
    else
	echo "Fatal error: cannot locate internal configuration file ($(basename $0):$LINENO)"
	exit 3
    fi
fi

source "$MT_SCRIPTS_PATH/mt_loadconf" "$MT_CONF_BASE"

# Set up colorization tools

# Define rule to remove color highlighting if required
if [ "$TERM" == "" ] || [ "$TERM" == "dumb" ]  || [ "$NOCOLOR" == "1" ] ; then
    COLORIZE=cat
    export COLORIZERED="$COLORIZE"
    export COLORIZEGREEN="$COLORIZE"
    export COLORIZEBLUE="$COLORIZE"
    export COLORIZECYAN="$COLORIZE"
    export COLORIZEYELLOW="$COLORIZE"
    export COLORIZEWHITE="$COLORIZE"
else
    COLORIZE=$MT_SCRIPTS_PATH/mt_colorize
    export COLORIZERED="$COLORIZE 1 31"
    export COLORIZEGREEN="$COLORIZE 1 32"
    export COLORIZEBLUE="$COLORIZE 1 34"
    export COLORIZECYAN="$COLORIZE 1 36"
    export COLORIZEYELLOW="$COLORIZE 1 33"
    export COLORIZEWHITE="$COLORIZE 1 29"
    color_red="`echo -e '\x1b[31;01m'`"
    color_green="`echo -e '\x1b[32;01m'`"
    color_blue="`echo -e '\x1b[34;01m'`"
    color_cyan="`echo -e '\x1b[36;01m'`"
    color_yellow="`echo -e '\x1b[33;01m'`"
    color_white="`echo -e '\x1b[29;01m'`"
    color_back="`echo -e '\x1b[00;00m'`"
fi

# Load utilies
source "$MT_SCRIPTS_PATH/mt_utils"

# Parse arguments
FAST=1
DEFAULTPIPELINE=2;
USEDEFAULT=0;
CLEAN=0;
DISTCLEAN=0;
PURGE=0;
UPDATE=0;
CONFIGURE=0;
VACUUM=0;
NOCOLOR=0;
DOCONVERT=0
TARGETS=all
while [ $# -ne 0 ]
do
    case $1 in
	fast)
	    echo "Option 'fast' is deprecated (ignored)" >&2
	    echo >&2
	    ;;
	clean)
	    CLEAN=1;
	    ;;
	vacuum)
	    VACUUM=1;
	    ;;
	distclean)
	    DISTCLEAN=1;
	    ;;
	purge)
	    PURGE=1;
	    ;;
	help | -h | --help)
	    usage;
	    exit 0;
	    ;;
	configure)
	    CONFIGURE=1;
	    ;;
	update)
	    UPDATE=1;
	    ;;
	version | -v | --version)
	    echo "MooseTeX $version ($year)"
	    exit 0
	    ;;
	--nocolor)
	    NOCOLOR=1;
	    ;;
	-d|--default)
	    USEDEFAULT=1;
	    ;;
	-f|--force)
	    if [ -f "$2" ] ; then
		touch "$2"
	    fi
	    shift
	    ;;
	-C)
	    WORK_PATH=$2;
	    WORK_PATH="`readlinkrec "$WORK_PATH"`"
	    shift
	    ;;
	-b|--backward)
	    TMP_PATH=$WORK_PATH
	    while [ "$TMP_PATH" != "/" ] ; do
		tmp="`supress_spaces "$HOME/.moosetex/$TMP_PATH/Makefile.rules"`"
		if [ -f "$tmp" ] && grep -q "CONFIGURATION_DONE=1" "$tmp" ; then
		    break;
		else
		    TMP_PATH="`readlinkrec "$TMP_PATH/.."`"
		fi
	    done
	    if [ "$TMP_PATH" != "/" ] ; then
		echo Select MooseTeX project: "$TMP_PATH" | $COLORIZEWHITE
		echo
	    fi
	    WORK_PATH="$TMP_PATH"
	    ;;
	convert)
	    DOCONVERT=1;
	    shift
	    convert_args="$@"
	    shift $#
	    ;;
	*.tex)
	    TARGETS="`echo "$@" | sed 's/\.tex/.pdf/g'`"
	    break
	    ;;
	*)
	    usage >& 2;
	    exit 1;
    esac
    shift
done
shift $(($OPTIND - 1))

# Define/create local moosetex configuration path
ROOT_CONF_PATH="$HOME/.moosetex"
if ! [ -d "$ROOT_CONF_PATH" ] ; then
    mkdir -p "$ROOT_CONF_PATH"
fi
MT_CONF="$ROOT_CONF_PATH/conf"
if ! [ -f "$MT_CONF" ] || [ "$UPDATE" == "1" ] ; then
    displaylogo

    printf "Configuration of MooseTeX\n" | $COLORIZERED
    printf "=========================\n" | $COLORIZERED
    printf "\n"

    if ! [ -f "$MT_CONF" ] ; then
	printf "This procedure is automatically started because it is the first time you\n"
	printf "use this version of MooseTeX with this account. This procedure wont be run\n"
	printf "again except if you type \"moosetex update\".\n\n"
    fi

    printf "MooseTeX is going to analyze the available software that can be used to\n"
    printf "generate images with specific formats. If a software is not unavailable,\n"
    printf "its associated feature is disabled. If you want to enable it, install the\n"
    printf "software, make sure it is available through your PATH environment variable,\n"
    printf "and type \"moosetex update\".\n\n"

    $MT_SCRIPTS_PATH/mt_update "$MT_CONF" | \
	$SEDUNBUFFERED -e "s/\(enabled\)/$color_green\1$color_back/g" |
	$SEDUNBUFFERED -e "s/\(disabled\)/$color_red\1$color_back/g"
    printf "\n"

    if [ "$UPDATE" == "1" ] ; then
	exit 0;
    fi
fi

# Examples
MT_DOC_PATH=$MT_SHAREDOC_PATH/doc.pdf

# Set up terminal to dummy to disable color highlighting
if [ "$NOCOLOR" == "1" ] ; then
    export TERM="dumb";
fi

# Source configuration file of moosetex's current project
source "$MT_SCRIPTS_PATH/mt_loadconf" "$MT_CONF"

# Determine number of threads
if [ "$FLOCK" == "" ] && [ "$LOCKFILE" == "" ]; then
    export MT_NBTHREADS=1
else
    export MT_NBTHREADS=8
fi

# Process to image conversion (for option convert only)
if [ "$DOCONVERT" == "1" ] ; then
    export ROOT_CONF_PATH=$ROOT_CONF_PATH
    export MT_SCRIPTS_PATH=$MT_SCRIPTS_PATH

    ((START_DAEMON=1 $MT_SCRIPTS_PATH/mt_convert $convert_args ; (echo $? > "$tmpfile.conv.status")) \
	3>&1 1>&2- 2>&3- | $COLORIZERED) 3>&1 1>&2- 2>&3-
    if [ -f "$tmpfile.conv.status" ] ; then
	res=`cat "$tmpfile.conv.status"`
    else
	res=3
    fi
    exit $res
fi

### Back to the main program

# Define/create local project configuration path
CONF_PATH="`supress_spaces "$HOME/.moosetex/$WORK_PATH/"`"
if ! [ -d "$CONF_PATH" ] ; then
    mkdir -p "$CONF_PATH"
fi

# Load compilation rules
MAKEFILE_RULES="$CONF_PATH/Makefile.rules"
if [ "$CONFIGURE" != "1" ] && [ -f "$MAKEFILE_RULES" ] ; then
    source "$MT_SCRIPTS_PATH/mt_loadconf" "$MAKEFILE_RULES"
fi

# Project configuration step
if [ "$CONFIGURE" == "1" ] || \
   [ "$CONFIGURATION_DONE" != "1" ] || \
   ! [ -f "$MAKEFILE_RULES" ] ; then

    displaylogo

    printf "Configuration of your current project\n" | $COLORIZERED
    printf "=====================================\n" | $COLORIZERED
    printf "\n"

    echo WORK_PATH=$WORK_PATH > "$MAKEFILE_RULES"

    printf "Configuration of LaTeX generation\n" | $COLORIZEGREEN
    printf "\n"

    printf "Do you want to generate PDF files using:\n";
    printf " 1. latex -> dvips -> ps2pdf, or\n";
    printf " 2. pdflatex.\n";
    question "Please enter your answer" "1/2" "$DEFAULTPIPELINE";
    PIPELINE=$answer;
    echo PIPELINE=$PIPELINE >> "$MAKEFILE_RULES"

    printf "\n";

    if [ "$PIPELINE" = "1" ] ; then
	IMGEXT=EPS;
	echo IMGEXT=$IMGEXT >> "$MAKEFILE_RULES"
	echo TARGETRULE=.tex.dvi >> "$MAKEFILE_RULES"
	echo LATEXCOMPILER=$LATEX >> "$MAKEFILE_RULES"
    else
	IMGEXT=PDF;
	echo IMGEXT=$IMGEXT >> "$MAKEFILE_RULES"
	echo TARGETRULE=.tex.pdf >> "$MAKEFILE_RULES"
	echo LATEXCOMPILER=$PDFLATEX >> "$MAKEFILE_RULES"
    fi

    printf "Configuration of image generation\n" | $COLORIZEGREEN
    printf "\n"

    # Image directory
    IMAGES_SRC_PATH=srcimages
    question_writabledir "What is your directory for source images?" "" "$IMAGES_SRC_PATH";
    IMAGES_SRC_PATH=$answer
    IMAGES_DIRECTIVES=$IMAGES_SRC_PATH/directives.conf
    echo IMAGES_SRC_PATH=$IMAGES_SRC_PATH >> "$MAKEFILE_RULES"
    echo IMAGES_DIRECTIVES=$IMAGES_DIRECTIVES >> "$MAKEFILE_RULES"

    # Built image directory
    IMAGES_DST_PATH=images
    question_writabledir "Where do you want to generate the target $IMGEXT images?" "" "$IMAGES_DST_PATH";
    IMAGES_DST_PATH="`echo $answer | sed 's $WORK_PATH/  '`";
    echo IMAGES_DST_PATH=$IMAGES_DST_PATH >> "$MAKEFILE_RULES"

    # Prebuilt image directory
    IMAGES_PREDST_PATH=prebuiltimages
    question_writabledir "Where do you want to place pre-built $IMGEXT images?" "" "$IMAGES_PREDST_PATH";
    IMAGES_PREDST_PATH="`echo $answer | sed 's ^$WORK_PATH/  '`";
    echo IMAGES_PREDST_PATH=$IMAGES_PREDST_PATH >> "$MAKEFILE_RULES"

    # Trash image directory
    IMAGES_TRASH_PATH=trashimages
    question_writabledir "Where do you want to place useless images (trash)?" "" "$IMAGES_TRASH_PATH";
    IMAGES_TRASH_PATH="`echo $answer | sed 's ^$WORK_PATH/  '`";
    echo IMAGES_TRASH_PATH=$IMAGES_TRASH_PATH >> "$MAKEFILE_RULES"

    # Format
    TARGET_FORMAT="`([ $PIPELINE == 1 ] && echo eps) || echo pdf`"
    echo TARGET_FORMAT=$TARGET_FORMAT >> "$MAKEFILE_RULES"
    question "Do you want DCT encoding (JPEG compression) of non-vectorial images?" "y/n" "n";
    if [ "$answer" == "y" ] ; then
	DCTENCODING=1;
    else
	DCTENCODING=0;
    fi
    echo DCTENCODING=$DCTENCODING >> "$MAKEFILE_RULES"

    printf '\n';

    printf "Ghostscript settings (image quality vs filesize):\n";
    printf " 1. screen (Poor quality, smallest filesize),\n";
    printf " 2. ebook (Low quality, small filesize),\n";
    printf " 3. printer (Good fidelity, large filesize),\n";
    printf " 4. prepress (Max doc fidelity, largest filesize).\n"
    question "Please enter your answer" "1/2/3/4" "4";
    case "$answer" in
	"1")
	    PDFSETTINGS="screen";
	    ;;
	"2")
	    PDFSETTINGS="ebook";
	    ;;
	"3")
	    PDFSETTINGS="printer";
	    ;;
	"4")
	    PDFSETTINGS="prepress";
	    ;;
    esac
    PS2PDF_SETTING="-dPDFSETTINGS=/$PDFSETTINGS"
    echo PS2PDF_SETTING=$PS2PDF_SETTING >> "$MAKEFILE_RULES"

    printf '\n';

    (
	echo COLORIZE=$COLORIZE

	case "`uname`" in
	    Linux)
		echo ECHO='/bin/echo -e'
		;;
	    Darwin)
		echo ECHO='echo'
		;;
	    SunOS)
		echo ECHO='/bin/echo'
		;;
	    *)
		echo ECHO='/bin/echo'
		;;
	esac
    ) >> "$MAKEFILE_RULES"

    # End project configuration
    printf "Configuration complete\n" | $COLORIZEGREEN;
    printf "======================\n" | $COLORIZEGREEN;
    printf "\n"

    (
	printf "You can specify to MooseTeX how to generate your images (resolution,\n";
	printf "boundingbox...) by indicating your directives in a file:\n";
	printf "\t$IMAGES_SRC_PATH/directives.conf\n";
	printf "\n";
	printf "If it is not already the case, please, add the following lines:\n";
	printf "\t\\\\usepackage{graphicx}\n";
	printf "\t\\\\graphicspath{{$IMAGES_DST_PATH/},{$IMAGES_PREDST_PATH/}}\n";
	printf "in your tex file(s) before the \\\begin{document}. Do not include image\n";
	printf "extensions when you refer to an image in commands such as \includegraphics.\n";
	printf "\n";
	printf "Please refer to the documentation for more details:\n";
	printf "\t`readlinkrec $MT_DOC_PATH`\n\n";
    )

    echo CONFIGURATION_DONE=1 >> "$MAKEFILE_RULES";
    if [ "$CONFIGURE" == "1" ] ; then
	exit 0;
    else
	source "$MT_SCRIPTS_PATH/mt_loadconf" "$MAKEFILE_RULES"
	printf "Press a key to proceed to the generation step\n"  | $COLORIZEWHITE;
	if [ "$USEDEFAULT" != "1" ] ; then
	    read dummy;
	else
	    echo
	fi
    fi
fi

# Define rule to remove trailing whitespace
RMTRAILINGWHITESPACE="$SEDUNBUFFERED -e s/[[:blank:]]*$//"

# Define function to postprocess standard output
POSTPROD="$RMTRAILINGWHITESPACE | grep --line-buffered -E -v '^make:.*|recipe for target .* failed'"

# Start compilation
export ROOT_CONF_PATH=$ROOT_CONF_PATH
export CONF_PATH=$CONF_PATH
export TEX_CACHE=$CONF_PATH
export MT_PATH=$MT_PATH
export MT_SHARE_PATH=$MT_SHARE_PATH
export MT_SCRIPTS_PATH=$MT_SCRIPTS_PATH

# Choose directive
if [ "$CLEAN" == "1" ] ; then
    make --no-print-directory -j $MT_NBTHREADS -f "$MT_SCRIPTS_PATH/mt_makefile" -C "$WORK_PATH" clean
    exit $?
fi

if [ "$DISTCLEAN" == "1" ] ; then
    make --no-print-directory -j $MT_NBTHREADS -f "$MT_SCRIPTS_PATH/mt_makefile" -C "$WORK_PATH" distclean
    exit $?
fi

if [ "$PURGE" == "1" ] ; then
    make --no-print-directory -j $MT_NBTHREADS -f "$MT_SCRIPTS_PATH/mt_makefile" -C "$WORK_PATH" purge
    exit $?
fi

if [ "$VACUUM" == "1" ] ; then
    make --no-print-directory -j $MT_NBTHREADS -f "$MT_SCRIPTS_PATH/mt_makefile" -C "$WORK_PATH" vacuum
    exit $?
fi

# Otherwise generate document
printf "Document generation\n" | $COLORIZERED;
printf "===================\n\n" | $COLORIZERED;

echo Loading...

# Generate dependancies
if ! make --no-print-directory -j $MT_NBTHREADS -k -f "$MT_SCRIPTS_PATH/mt_makefile" -C "$WORK_PATH" depend ; then
    echo "Fata error: cannot generate dependencies ($(basename $0):$LINENO)" 1>&2
    exit 3;
fi

# Analyze the compilation plan
code="$tmpfile.code"
if ! make --no-print-directory -j $MT_NBTHREADS -n -k -f "$MT_SCRIPTS_PATH/mt_makefile" -C "$WORK_PATH" all > $code ; then
    echo "Fatal error: cannot generate compilation plan ($(basename $0):$LINENO)" 1>&2
    exit 3;
fi

# Loads soft utils before running daemons
source "$MT_SCRIPTS_PATH/mt_daemonsofts"

# Run daemons if necessary
cd "$WORK_PATH"
if [ "$GEOGEBRA" != "" ] && [ -d "$IMAGES_SRC_PATH" ] ; then
    while read file ; do
	ext="`$MT_SCRIPTS_PATH/mt_getextension "$file"`";
	if grep -q "$file" "$code" ; then
	    start_xvfb_daemon
	    break;
	fi
    done < <(find "$IMAGES_SRC_PATH" -name '*.ggb' -o -name '*.xmi')
fi
if [ "$MATLAB" != "" ] && [ -d "$IMAGES_SRC_PATH" ] ; then
    while read file ; do
	ext="`$MT_SCRIPTS_PATH/mt_getextension "$file"`";
	if ([ "$ext" == "m" ] || \
	    (head -1 "$file" | grep -q "MATLAB") 2>/dev/null) && \
	    grep -q "$file" "$code" ; then
	    start_matlab_daemon
	    break;
	fi
    done < <(find "$IMAGES_SRC_PATH" -name '*.fig' -o -name '*.m')
fi
if [ "$OCTAVE" != "" ] && [ -d "$IMAGES_SRC_PATH" ] ; then
    while read file ; do
	ext="`$MT_SCRIPTS_PATH/mt_getextension "$file"`";
	if grep -q "$file" "$code" ; then
	    start_octave_daemon
	    break;
	fi
    done < <(find "$IMAGES_SRC_PATH" -name '*.oct' -o -name '*.octave')
fi
if [ "$PYTHON" != "" ] && [ -d "$IMAGES_SRC_PATH" ] ; then
    while read file ; do
	if grep -q "$file" "$code" ; then
	    if [ "$XVFB" ] ; then
		start_xvfb_daemon
	    fi
	    start_python_daemon
	    break;
	fi
    done < <(find "$IMAGES_SRC_PATH" -name '*.py' -o -name '*.pickle')
fi
if [ "$SCILAB" != "" ] && [ -d "$IMAGES_SRC_PATH" ] ; then
    while read file ; do
	if grep -q "$file" "$code" ; then
	    if [ "`echo "$SCILAB_VERSION < 5.5" | bc`" == "1" ] ; then
		start_xvfb_daemon
	    fi
	    start_scilab_daemon
	    break;
	fi
    done < <(find "$IMAGES_SRC_PATH" -name '*.scg' -o -name '*.sce' -o -name '*.sci')
fi
if [ "$R" != "" ] && [ -d "$IMAGES_SRC_PATH" ] ; then
    while read file ; do
	if grep -q "$file" "$code" ; then
	    start_R_daemon
	    break;
	fi
    done < <(find "$IMAGES_SRC_PATH" -name '*.r' -o -name '*.R')
fi
cd - > /dev/null
echo

# Run Compilation
printf "%-50.50s Comments\n" "Generated files" | $COLORIZEWHITE
printf "%*s\n" 100 | tr " " "=" | $COLORIZEWHITE
mispipe "make --no-print-directory -j $MT_NBTHREADS -k -f '$MT_SCRIPTS_PATH/mt_makefile' \
    -C '$WORK_PATH' $TARGETS 2>&1" "$POSTPROD"
res=$?
printf "%*s\n" 100 | tr " " "=" | $COLORIZEWHITE
echo

# Stop daemons
echo Closing...
stop_R_daemon
stop_scilab_daemon
stop_python_daemon
stop_octave_daemon
stop_matlab_daemon
stop_xvfb_daemon

echo

UNDEFINED='((Reference|Citation).*undefined)|(Label.*multiply defined)'

# Parse errors
cd "$WORK_PATH"
while read file ; do
    if grep -q -E -i -e "$UNDEFINED" -e '^LaTeX Warning:' "$file" ; then
	texfile="$($MT_SCRIPTS_PATH/mt_removeextension "$file").tex"
	echo "$color_green$texfile:1:$color_back"
	grep -E -m20 -i -e "$UNDEFINED" -e '^LaTeX Warning:' "$file" | grep '^..*$'
	echo '--' | $COLORIZERED;
	echo 'More details in' "$file" | $COLORIZEYELLOW
	echo
    fi
done < <(find . -name '*.log') > $tmpfile.warn.log 2>/dev/null
while read file ; do
    if [ "$NOCOLOR" == "0" ] ; then
	texfile="$($MT_SCRIPTS_PATH/mt_removeextension "$file").tex"
	nblines=`wc -l $texfile | cut -d' ' -f 1`
	if mispipe "grep -E -m4 -A10 '[[:graph:]]*:[0-9]*:|^!|No pages of output' '$file'"	\
	    "grep -v '^[[:blank:]]*$' | $SEDUNBUFFERED						\
		 -e 's|^\(!\)|$texfile:$nblines: \1|g'				       		\
	         -e 's/\([[:graph:]]*:[0-9]*:\)/$color_green\1$color_back/g'			\
	         -e 's/\(.*--\)/$color_red\1$color_back/g'" ; then
	    echo '--' | $COLORIZERED;
	    echo 'More details in' "$file" | $COLORIZEYELLOW
	    echo
	fi
    else
	if mispipe "grep -E -m4 -A10 '[[:graph:]]*:[0-9]*:|^!|No pages of output' '$file'"	\
	    "grep -v '^[[:blank:]]*$' | $SEDUNBUFFERED" ; then
	    echo '--' | $COLORIZERED;
	    echo 'More details in' "$file" | $COLORIZEYELLOW
	    echo
	fi
    fi
done < <(find . -name '*.log') > $tmpfile.error.log 2>/dev/null
while read file ; do
    if grep -q -E -v -e 'I found no|(There were .*)|(There was .*)' "$file" ; then
	bibfile=${file:0:-8}.tex
	echo "$color_green$bibfile:1:$color_back"
	grep -E -v -e 'I found no|(There were .*)|(There was .*)' "$file" | grep -E -m20 '^..*$'
	echo '--' | $COLORIZERED;
	echo 'More details in' "$file" | $COLORIZEYELLOW
	echo
    fi
done < <(find . -name '*.bib.log') >> $tmpfile.warn.log 2>/dev/null
if grep '..*' "$CONF_PATH/mt_deps.log" ; then
    echo
fi > $tmpfile.mt.log 2>/dev/null
if grep -q '^..*$' "$tmpfile.mt.log" ; then
    echo Following MooseTeX warnings occured | $COLORIZERED;
    echo =================================== | $COLORIZERED;
    echo
    cat $tmpfile.mt.log  | $SEDUNBUFFERED -e "s/^\(Type.*\)$/$color_yellow\1$color_back/g"
fi
if grep -q '^..*$' "$tmpfile.warn.log" ; then
    echo Following LaTeX/BibTeX warnings occured | $COLORIZERED;
    echo ======================================= | $COLORIZERED;
    echo
    cat $tmpfile.warn.log
fi
if grep -q '^..*$' "$tmpfile.error.log" ; then
    echo Following LaTeX/BibTeX errors occured | $COLORIZERED;
    echo ===================================== | $COLORIZERED;
    echo
    cat $tmpfile.error.log
fi
cd - > /dev/null

exit $res
